package clickhouse

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"sync/atomic"
	"time"
)

type BatchRow interface {
	InsertQuery() string
	StmtName() string
}

type Store interface {
	Add(rows ...BatchRow)
	GetDB() *sqlx.DB
	//GetBatches() int32
	Errors() <-chan error
	Close()
}

const (
	batchLimit    = uint16(100)
	batchRuns     = int32(1)
	batchDeadline = time.Second
)

type store struct {
	db *sqlx.DB

	rows chan BatchRow
	done chan struct{}

	errors chan error

	batchRuns     *int32
	batchLimit    uint16
	batchDeadline time.Duration
}

type Option func(s *store)

func BatchLimit(limit uint16) Option {
	return func(s *store) {
		s.batchLimit = limit
	}
}

func SetDB(db *sql.DB) Option {
	return func(s *store) {
		s.db = sqlx.NewDb(db, "clickhouse")
	}
}

func BatchRuns(runs int32) Option {
	return func(s *store) {
		s.batchRuns = &runs
	}
}

func BatchDeadline(deadline time.Duration) Option {
	return func(s *store) {
		s.batchDeadline = deadline
	}
}

func New(options ...Option) Store {
	runs := batchRuns
	s := &store{
		rows:          make(chan BatchRow),
		done:          make(chan struct{}),
		errors:        make(chan error),
		batchRuns:     &runs,
		batchLimit:    batchLimit,
		batchDeadline: batchDeadline,
	}

	for _, option := range options {
		option(s)
	}

	atomic.StoreInt32(s.batchRuns, runs)

	for i := int32(0); i < *s.batchRuns; i++ {
		go s.run()
	}

	return s
}

func (s *store) Close() {
	for i := int32(0); i < *s.batchRuns; i++ {
		s.done <- struct{}{}
	}

	tick := time.Tick(time.Millisecond)

	for range tick {
		runs := atomic.LoadInt32(s.batchRuns)
		if runs == 0 {
			break
		}
	}

	close(s.rows)
	close(s.errors)
	close(s.done)
}

//func (s store) GetBatches() int32 {
//	return *s.batchRuns
//}

func (s *store) Add(rows ...BatchRow) {
	for _, row := range rows {
		s.rows <- row
	}
}

func (s *store) Errors() <-chan error {
	return s.errors
}

func (s *store) run() {
	defer func() {
		atomic.AddInt32(s.batchRuns, -1)
	}()

	var (
		tr         *sqlx.Tx
		after      <-chan time.Time
		row        BatchRow
		err        error
		batchCount uint16

		stmts = make(map[string]*sqlx.NamedStmt)
	)

	commit := func() {
		if tr != nil {
			errTr := tr.Commit()
			if errTr != nil {
				errRb := tr.Rollback()
				if errRb != nil {
					s.errors <- errors.Wrap(errRb, "rollback failed")
				}

				s.errors <- errors.Wrap(errTr, "commit failed")
			}
		}

		tr, after, batchCount = nil, nil, 0
	}

	for {
		if after == nil {
			after = time.After(s.batchDeadline)
		}

		select {
		case row = <-s.rows:
			_ = row
		case <-after:
			commit()
			continue
		case <-s.done:
			commit()
			return
		}

		if tr == nil && row != nil {
			tr, err = s.db.Beginx()
			if err != nil {
				s.errors <- errors.Wrap(err, "create transaction failed")

				return
			}

			stmt, err := tr.PrepareNamed(row.InsertQuery())
			if err != nil {
				s.errors <- errors.Wrap(err, "prepare named statement failed")

				return
			}

			stmts[row.StmtName()] = stmt
		}

		if row != nil {
			stmt, ok := stmts[row.StmtName()]
			if !ok {
				s.errors <- errors.Errorf("named statement %s not found", row.StmtName())

				return
			}

			_, err = stmt.Exec(row)
			if err != nil {
				s.errors <- errors.Wrap(err, "execute name statement failed")

				return
			}

			batchCount++
		}

		if batchCount >= s.batchLimit {
			commit()
		}
	}
}

func (s *store) GetDB() *sqlx.DB {
	return s.db
}
