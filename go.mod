module gitlab.com/adsys1/stat/clickhouse.git

go 1.14

require (
	github.com/jmoiron/sqlx v1.2.0
	github.com/partyzanex/testutils v0.0.6
	github.com/pkg/errors v0.9.1
)
