CREATE DATABASE IF NOT EXISTS adsys;

CREATE TABLE adsys.users
(
    UID  FixedString(26),
    Date Date,
    Time DateTime
) ENGINE = MergeTree()
  PARTITION BY toYYYYMM(Date)
  ORDER BY (UID, Date, Time)
  PRIMARY KEY (UID, Date, Time);

CREATE TABLE IF NOT EXISTS adsys.usersd AS adsys.users
ENGINE = Distributed(stat, adsys, users, sipHash64(UID));

