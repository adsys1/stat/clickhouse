package clickhouse_test

import (
	"github.com/partyzanex/testutils"
	"gitlab.com/adsys1/stat/clickhouse.git"
	"sync"
	"testing"
	"time"
)

func TestNewUserStore(t *testing.T) {
	db := testutils.NewSqlDB(t, "clickhouse", "CH_CLUSTER_TEST")

	store := clickhouse.New(clickhouse.SetDB(db), clickhouse.BatchRuns(1))

	errs := store.Errors()

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()

		for err := range errs {
			testutils.Err(t, "store error", err)
		}
	}()

	users := make([]clickhouse.BatchRow, 500)

	for i := 0; i < 500; i++ {
		users[i] = getTestUser()
	}

	store.Add(users...)
	store.Close()

	wg.Wait()

	dbx := store.GetDB()

	time.Sleep(time.Second)

	for _, user := range users {
		exp := user.(*clickhouse.User)
		got := &clickhouse.User{}

		err := dbx.Get(got, `SELECT * FROM adsys.usersd WHERE UID = ?`, exp.UID)
		testutils.Err(t, "dbx.Get", err)

		equalUser(t, exp, got)
	}
}

func equalUser(t testutils.Tester, exp, got *clickhouse.User) {
	testutils.AssertEqual(t, "UID", exp.UID, got.UID)
	testutils.AssertEqual(t, `Date.Format("20060102")`, exp.Date.Format("20060102"), got.Date.Format("20060102"))
	testutils.AssertEqual(t, "Time.Unix()", exp.Time.Unix(), got.Time.Unix())
}

func getTestUser() *clickhouse.User {
	return &clickhouse.User{
		UID:  testutils.RandomString(26),
		Date: time.Now(),
		Time: time.Now(),
	}
}

func BenchmarkUserStore_Add(b *testing.B) {
	db := testutils.NewSqlDB(b, "clickhouse", "CH_CLUSTER_TEST")

	store := clickhouse.New(
		clickhouse.SetDB(db),
		clickhouse.BatchRuns(2),
		clickhouse.BatchLimit(2000),
	)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		//b.StopTimer()
		user := getTestUser()
		//b.StartTimer()

		store.Add(user)
	}

	store.Close()
}

func BenchmarkUserStore_AddParallel(b *testing.B) {
	db := testutils.NewSqlDB(b, "clickhouse", "CH_CLUSTER_TEST")

	store := clickhouse.New(
		clickhouse.SetDB(db),
		clickhouse.BatchRuns(2),
		clickhouse.BatchLimit(2000),
	)

	b.ResetTimer()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			user := getTestUser()
			store.Add(user)
		}
	})

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		user := getTestUser()
		b.StartTimer()

		store.Add(user)
	}

	store.Close()
}
