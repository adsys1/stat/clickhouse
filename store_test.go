package clickhouse_test

import (
	"database/sql"
	"github.com/partyzanex/testutils"
	"gitlab.com/adsys1/stat/clickhouse.git"
	"sync"
	"testing"
	"time"
)

type testEvent struct {
	ID    string    `db:"ID"`
	SubID string    `db:"SubID"`
	IntID int64     `db:"IntID"`
	Date  time.Time `db:"Date"`
	Time  time.Time `db:"Time"`
}

func (testEvent) InsertQuery() string {
	return `INSERT INTO test_events (ID, SubID, IntID, Date, Time) 
VALUES (:ID, :SubID, :IntID, :Date, :Time)`
}

func (testEvent) StmtName() string {
	return "test_events"
}

func table(t testutils.Tester, db *sql.DB) func() {
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS test_events (
		ID FixedString(32),
		SubID FixedString(32),
		IntID Int64,
		Date Date,
		Time DateTime
	) ENGINE = MergeTree() ORDER BY (Date, Time)`)
	testutils.FatalErr(t, "create table", err)

	fn := func() {
		_, err := db.Exec(`DROP TABLE test_events`)
		testutils.FatalErr(t, "drop table", err)
	}

	return fn
}

// export CH_TEST="tcp://127.0.0.1:9000?database=test"
func TestStore(t *testing.T) {
	db := testutils.NewSqlDB(t, "clickhouse", "CH_TEST")

	drop := table(t, db)
	defer drop()

	wait := time.After(1200 * time.Millisecond)

	store := clickhouse.New(clickhouse.SetDB(db), clickhouse.BatchRuns(1))

	errs := store.Errors()

	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()

		for err := range errs {
			testutils.Err(t, "store error", err)
		}
	}()

	exp := getTestEvent()

	store.Add(exp)

	<-wait

	dbx := store.GetDB()

	got := &testEvent{}

	err := dbx.Get(got, `SELECT * FROM test_events WHERE ID = ?`, exp.ID)
	testutils.Err(t, "dbx.Get", err)

	equalTestEvent(t, exp, got)

	events := make([]clickhouse.BatchRow, 500)

	for i := 0; i < 500; i++ {
		events[i] = getTestEvent()
	}

	store.Add(events...)
	store.Close()

	for _, event := range events {
		exp := event.(*testEvent)
		got := &testEvent{}

		err := dbx.Get(got, `SELECT * FROM test_events WHERE ID = ?`, exp.ID)
		testutils.Err(t, "dbx.Get", err)

		equalTestEvent(t, exp, got)
	}

	wg.Wait()
}

func equalTestEvent(t testutils.Tester, exp, got *testEvent) {
	testutils.AssertEqual(t, "ID", exp.ID, got.ID)
	testutils.AssertEqual(t, "SubID", exp.SubID, got.SubID)
	testutils.AssertEqual(t, "IntID", exp.IntID, got.IntID)
	testutils.AssertEqual(t, `Date.Format("20060102")`, exp.Date.Format("20060102"), got.Date.Format("20060102"))
	testutils.AssertEqual(t, "Time.Unix()", exp.Time.Unix(), got.Time.Unix())
}

func getTestEvent() *testEvent {
	now := time.Now()
	return &testEvent{
		ID:    testutils.RandomString(32),
		SubID: testutils.RandomString(32),
		IntID: testutils.RandInt64(-10000, 10000),
		Date:  now,
		Time:  now,
	}
}

func BenchmarkStore_Add(b *testing.B) {
	db := testutils.NewSqlDB(b, "clickhouse", "CH_TEST")

	drop := table(b, db)
	_ = drop
	//defer drop()

	store := clickhouse.New(
		clickhouse.SetDB(db),
		clickhouse.BatchLimit(1000),
		clickhouse.BatchRuns(4),
	)

	event := getTestEvent()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		store.Add(event)
	}

	store.Close()
}

func BenchmarkStore_AddParallel(b *testing.B) {
	db := testutils.NewSqlDB(b, "clickhouse", "CH_TEST")

	drop := table(b, db)
	_ = drop
	//defer drop()

	store := clickhouse.New(
		clickhouse.SetDB(db),
		clickhouse.BatchLimit(1000),
		clickhouse.BatchRuns(4),
	)

	event := getTestEvent()

	b.ResetTimer()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			store.Add(event)
		}
	})

	store.Close()
}
