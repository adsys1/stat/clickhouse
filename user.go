package clickhouse

import "time"

type User struct {
	UID  string    `db:"UID" json:"uid"`
	Date time.Time `db:"Date" json:"date"`
	Time time.Time `db:"Time" json:"time"`
}

func (User) InsertQuery() string {
	return `INSERT INTO adsys.usersd (UID, Date, Time) VALUES (:UID, :Date, :Time)`
}

func (User) StmtName() string {
	return "users"
}

//
//type UserStore struct {
//	Store
//}
//
//func NewUserStore(options ...Option) *UserStore {
//	return &UserStore{
//		Store: New(options...),
//	}
//}
